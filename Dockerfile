FROM microsoft/dotnet:2.2-sdk

WORKDIR /source

COPY ./HomeWork1/ ./HomeWork1/
COPY ./HomeWork1.sln .
COPY nuget.config .

RUN dotnet publish ./HomeWork1/HomeWork1.csproj --output /app

WORKDIR /app
RUN rm -rf /source

ENTRYPOINT ["/usr/bin/dotnet", "/app/HomeWork1.dll"]