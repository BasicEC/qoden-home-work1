﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeWork1.Migrations
{
    public partial class RenameColumnInMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sended_at",
                table: "messages",
                newName: "sent_at");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sent_at",
                table: "messages",
                newName: "sended_at");
        }
    }
}
