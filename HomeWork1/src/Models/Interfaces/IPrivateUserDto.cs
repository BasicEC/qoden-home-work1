namespace HomeWork1.Models.Interfaces
{
    public interface IPrivateUserDto : IUserDto
    {
        string Description { get; set; }
    }
}