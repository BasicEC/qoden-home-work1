namespace HomeWork1.Models.Interfaces
{
    public interface IUserDto
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Patronymic { get; set; }
        string Nickname { get; set; }
        string Email { get; set; }
        string PhoneNumber { get; set; }
        string Department { get; set; }
    }
}