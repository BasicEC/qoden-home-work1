using HomeWork1.Models.Interfaces;

namespace HomeWork1.Models.Requests
{
    public class RateRequestRequest
    {
        public int Rate { get; set; }
        public string Description { get; set; }
    }
}