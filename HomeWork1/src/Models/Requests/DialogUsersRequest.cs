using System;
using System.Collections.Generic;

namespace HomeWork1.Models.Requests
{
    public class DialogUsersRequest
    {
        public Guid DialogId { get; set; }
        public string ManagerNickname { get; set; }
        public List<string> Nicknames { get; set; }
    }
}