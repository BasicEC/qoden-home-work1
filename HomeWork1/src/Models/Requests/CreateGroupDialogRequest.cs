using System.Collections.Generic;
using HomeWork1.Database.Models;

namespace HomeWork1.Models.Requests
{
    public class CreateGroupDialogRequest
    {
        public string Name { get; set; }
        public List<string> Nicknames { get; set; }
    }
}