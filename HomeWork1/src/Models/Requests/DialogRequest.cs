using System;

namespace HomeWork1.Models.Requests
{
    public class DialogRequest
    {
        public Guid DialogId { get; set; }
    }
}