using System.Net.NetworkInformation;

namespace HomeWork1.Models.Requests
{
    public class AssignRequest
    {
        public string ManagerNickname { get; set; }
        public string UserNickname { get; set; }
    }
}