namespace HomeWork1.Models.Requests
{
    public class CreatePersonalDialogRequest
    {
        public string Name { get; set; }
        public string Nickname { get; set; }
    }
}