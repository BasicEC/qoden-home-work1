using HomeWork1.Models.Interfaces;

namespace HomeWork1.Models.Requests
{
    public class ModifyProfileRequest : IPrivateUserDto
    {
        public string CurrentNickname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
        public string Description { get; set; }
    }
}