using HomeWork1.Database.Models;
using HomeWork1.Models.Interfaces;

namespace HomeWork1.Models
{
    public class UserDto : IPrivateUserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Department { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }

        public UserDto()
        {
        }

        public UserDto(User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Patronymic = user.Patronymic;
            Nickname = user.Nickname;
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            Department = user.Department.Name;
            Description = user.Description;
        }
    }
}