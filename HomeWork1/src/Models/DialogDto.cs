using System;
using HomeWork1.Database.Models;

namespace HomeWork1.Models
{
    public class DialogDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DialogType Type { get; set; }
    }
}