using System;

namespace HomeWork1.Models
{
    public class MessageDto
    {
        public string Text { get; set; }
        public Guid DialogId { get; set; }
        public string Sender { get; set; }
        public DateTime SentAt { get; set; }
    }
}