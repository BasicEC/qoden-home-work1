using System;
using HomeWork1.Database.Models;
using HomeWork1.Models.Interfaces;

namespace HomeWork1.Models
{
    public class RateRequestDto
    {
        public string UserNickname { get; set; }
        public int Rate { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Description { get; set; }
        public string ReviewerNickName { get; set; }
        public string ReviewerReply { get; set; }
        public string ReviewerComment { get; set; }
        public string Status { get; set; }
        public Guid RequestId { get; set; }        

        public RateRequestDto()
        {
        }

        public RateRequestDto(RateRequest rateRequest)
        {
            RequestId = rateRequest.RequestId;
            UserNickname = rateRequest.User?.Nickname;
            Rate = rateRequest.Rate;
            CreatedAt = rateRequest.CreatedAt;
            Description = rateRequest.Description;
            ReviewerNickName = rateRequest.Reviewer?.Nickname;
            ReviewerReply = rateRequest.ReviewerReply;
            ReviewerComment = rateRequest.ReviewerComment;
            Status = rateRequest.Status.ToString();
        }
    }
}