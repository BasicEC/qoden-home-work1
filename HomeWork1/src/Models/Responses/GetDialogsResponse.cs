using System.Collections.Generic;

namespace HomeWork1.Models.Responses
{
    public class GetDialogsResponse
    {
        public List<DialogDto> Dialogs { get; set; }
    }
}