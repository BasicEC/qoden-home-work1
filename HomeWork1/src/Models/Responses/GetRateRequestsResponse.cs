using System.Collections.Generic;

namespace HomeWork1.Models.Responses
{
    public class GetRateRequestsResponse
    {
        public List<RateRequestDto> RateRequests { get; set; }
    }
}