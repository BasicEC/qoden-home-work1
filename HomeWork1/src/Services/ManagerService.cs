using System;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Constants;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Microsoft.EntityFrameworkCore;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface IManagerService
    {
        Task AssignUserToManager(string managerNickname, string userNickname);
    }
    
    public class ManagerService : IManagerService
    {
        private PgDbContext _db;
        

        public ManagerService(PgDbContext db)
        {
            _db = db;
        }
        
        public async Task AssignUserToManager(string managerNickname, string userNickname)
        {
            var users = await _db.GetUsersByNickNames(managerNickname, userNickname);
            var manager = users.FirstOrDefault(u => u.Nickname == managerNickname);
            var user = users.FirstOrDefault(u => u.Nickname == userNickname);

            var validator = new Validator();
            validator.IsFound(manager, nameof(managerNickname),managerNickname, nameof(user));
            validator.IsFound(user, nameof(userNickname),userNickname, nameof(manager));
            validator.Throw();
            
            var managerRoles = await _db.GetUserRolesById((int)manager.Id);
            Check.Value(managerRoles.Exists(r => r.Name == RoleTypes.Manager || r.Name == RoleTypes.Admin))
                .EqualsTo(true, $"{managerNickname} isn't a manager or Admin");

            var managerAssignedUser = await _db.ManagerAssignedUsers
                .FirstOrDefaultAsync(m => m.Status == ManagerStatus.Relevant && m.AssignedUserId == user.Id);

            if (managerAssignedUser != null)
            {
                Check.Value(managerAssignedUser.ManagerId)
                    .NotEqualsTo(manager.Id, $"{managerNickname} is already a {userNickname} manager");
                managerAssignedUser.Status = ManagerStatus.Irrelevant;
                _db.ManagerAssignedUsers.Update(managerAssignedUser);    
            }
            
            _db.ManagerAssignedUsers.Add(
                new ManagerAssignedUser
                {
                    ManagerId = manager.Id,
                    AssignedUserId = user.Id,
                    AssignedAt = DateTime.Now,
                    Status = ManagerStatus.Relevant,
                });
            
            await _db.SaveChangesAsync();
        }
    }
}