using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Qoden.Validation;
using Validator = Qoden.Validation.Validator;

namespace HomeWork1.Services
{
    public interface IDialogService
    {
        Task<DialogDto> CreatePersonalDialog(CreatePersonalDialogRequest request, string nickname);
        Task<DialogDto> CreateGroupDialog(CreateGroupDialogRequest request, string nickname);
        Task<List<DialogDto>> GetUserDialogs(int userId);
        Task LeaveDialog(DialogRequest request, int userId);
        Task DeleteDialog(Guid dialogId, string nickname);
    }

    public class DialogService : IDialogService
    {
        private Validator _check;
        private Validator _validator;
        private PgDbContext _db;
        private IUserDialogService _userDialogService;

        public DialogService(PgDbContext db, IUserDialogService userDialogService)
        {
            _check = new Validator(true);
            _validator = new Validator();
            _db = db;
            _userDialogService = userDialogService;
        }

        public async Task<DialogDto> CreatePersonalDialog(CreatePersonalDialogRequest request, string nickname)
        {
            _check.CheckValue(request, nameof(request)).NotNull();

            _validator.CheckValue(request.Nickname).NotEmpty();
            _validator.NotEmptyOrNull(nameof(request.Name), request.Name);
            _validator.CheckValue(nickname).NotEmpty();
            _validator.Throw();

            return await CreatePersonalDialog(nickname, request.Nickname, request.Name);
        }

        private async Task<DialogDto> CreatePersonalDialog(string firstNickname, string secondNickname, string Name)
        {
            var users = await _db.GetUsersByNickNames(firstNickname, secondNickname);
            var fUser = users.FirstOrDefault(u => u.Nickname == firstNickname);
            var sUser = users.FirstOrDefault(u => u.Nickname == secondNickname);

            _validator.IsFound(fUser, nameof(firstNickname), firstNickname, nameof(User));
            _validator.IsFound(sUser, nameof(secondNickname), secondNickname, nameof(User));
            _validator.Throw();

            var dialog = await _db.GetPersonalDialogByUsersId(fUser.Id, sUser.Id);
            _check.NotFound(dialog, "users", $"{firstNickname}, {secondNickname}", nameof(Dialog));

            dialog = new Dialog
            {
                Name = Name,
                Type = DialogType.Personal,
                Id = Guid.NewGuid(),
            };

            var fUserDialog = new UserDialog
            {
                UserId = fUser.Id,
                DialogId = dialog.Id
            };

            var sUserDialog = new UserDialog
            {
                UserId = sUser.Id,
                DialogId = dialog.Id
            };

            _db.Dialogs.Add(dialog);
            _db.UserDialogs.Add(fUserDialog);
            _db.UserDialogs.Add(sUserDialog);
            await _db.SaveChangesAsync();

            return new DialogDto
            {
                Id = dialog.Id,
                Name = dialog.Name,
                Type = dialog.Type,
            };
        }

        public async Task<DialogDto> CreateGroupDialog(CreateGroupDialogRequest request, string nickname)
        {
            _check.CheckValue(request, nameof(request)).NotNull();
            _check.CheckValue(request.Nicknames.Count, nameof(request.Nicknames.Count)).Greater(0);
            _check.CheckValue(nickname, nameof(nickname)).NotEmpty();
            var dialogDto = await CreateGroupDialog(nickname, request.Name);
            await _userDialogService.AddUsers(
                new DialogUsersRequest
                {
                    DialogId = dialogDto.Id,
                    ManagerNickname = nickname,
                    Nicknames = request.Nicknames,
                });     
            return dialogDto;
        }

        private async Task<DialogDto> CreateGroupDialog(string nickname, string name)
        {
            var user = await _db.GetUserByNickName(nickname);
            _check.IsFound(user, nameof(nickname), nickname, nameof(User));

            var dialog = new Dialog
            {
                Name = name,
                Id = Guid.NewGuid(),
                Type = DialogType.Group,
            };

            var userDialog = new UserDialog
            {
                UserId = user.Id,
                DialogId = dialog.Id,
                Role = UserDialogRole.Admin,
            };

            _db.Dialogs.Add(dialog);
            _db.UserDialogs.Add(userDialog);
            await _db.SaveChangesAsync();
            return new DialogDto
            {
                Id = dialog.Id,
                Name = dialog.Name,
                Type = dialog.Type,
            };
        }

        public async Task LeaveDialog(DialogRequest request, int userId)
        {
            _check.CheckValue(request).NotNull();

            var userDialog = await _db.GetUserDialogIncludeDialog(userId, request.DialogId);

            _check.GroupDialogIsFoundById(userDialog.Dialog, request.DialogId);
            _check.CheckValue(userDialog.Role, nameof(userDialog.Role)).NotEqualsTo(UserDialogRole.Admin);

            _db.Remove(userDialog);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteDialog(Guid dialogId, string nickname)
        {            
            var dialog = await _db.GetGroupDialogByIdIncludeUsers(dialogId);
            _check.GroupDialogIsFoundById(dialog, dialogId);

            var userDialog = _check.UserIsFoundInDialog(nickname, dialog.Users);
            _check.CheckValue(userDialog.Role).EqualsTo(UserDialogRole.Admin);

            _db.Dialogs.Remove(dialog);
            await _db.SaveChangesAsync();
        }

        public async Task<List<DialogDto>> GetUserDialogs(int userId)
        {
            var dialogs = await _db.GetDialogsByUserId(userId);
            return dialogs
                .Select(d => new DialogDto
                {
                    Id = d.Id,
                    Name = d.Name,
                    Type = d.Type,
                })
                .ToList();
        }

        

        
    }
}