using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface IUserDialogService
    {
        Task AddUsers(DialogUsersRequest request);
        Task RemoveUsers(DialogUsersRequest request);
        Task AddManagers(DialogUsersRequest request, string nickname);
        Task RemoveManagers(DialogUsersRequest request, string nickname);
    }
    
    public class UserDialogService : IUserDialogService
    {
        private Validator _check;
        private Validator _validator;
        private PgDbContext _db;

        public UserDialogService(PgDbContext db)
        {
            _check = new Validator(true);
            _validator = new Validator();
            _db = db;
        }
        
        public async Task AddUsers(DialogUsersRequest request)
        {
            CheckDialogUsersRequest(request);
            await AddUsers(request.DialogId, request.ManagerNickname, request.Nicknames);
        }

        private async Task AddUsers(Guid dialogId, string managerNickname, List<string> nicknames)
        {
            var dialog = await _db.GetGroupDialogByIdIncludeUsers(dialogId);
            _check.GroupDialogIsFoundById(dialog, dialogId);

            var managerUserDialog = _check.UserIsFoundInDialog(managerNickname, dialog.Users);
            _check.DialogRoleIsManagerOrAdmin(managerUserDialog);

            var users = await _db.GetUsersByNickNames(nicknames.ToArray());
            _validator.UsersFound(users, nicknames);
            _validator.Throw();

            _validator.UsersNotFoundInDialog(dialog.Users, nicknames);
            _validator.Throw();

            foreach (var user in users)
            {
                var userDialog = new UserDialog
                {
                    UserId = user.Id,
                    DialogId = dialogId,
                    Role = UserDialogRole.User,
                };
                _db.UserDialogs.Add(userDialog);
            }

            await _db.SaveChangesAsync();
        }

        public async Task RemoveUsers(DialogUsersRequest request)
        {
            CheckDialogUsersRequest(request);
            await RemoveUsers(request.DialogId, request.ManagerNickname, request.Nicknames);
        }

        private async Task RemoveUsers(Guid dialogId, string managerNickname, List<string> nicknames)
        {
            var dialog = await _db.GetGroupDialogByIdIncludeUsers(dialogId);
            _check.GroupDialogIsFoundById(dialog, dialogId);

            var managerUserDialog = _check.UserIsFoundInDialog(managerNickname, dialog.Users);
            _check.DialogRoleIsManagerOrAdmin(managerUserDialog);

            _check.CheckValue(nicknames.Contains(managerNickname)).IsFalse("User can't remove himself");

            var userDialogsToRemove = _validator.UsersFoundInDialog(dialog.Users, nicknames);
            _validator.Throw();

            if (managerUserDialog.Role == UserDialogRole.Manager)
            {
                _validator.UserDialogsHaveNoManagersOrAdmin(userDialogsToRemove);
                _validator.Throw();
            }

            _db.UserDialogs.RemoveRange(userDialogsToRemove);
            await _db.SaveChangesAsync();
        }
        
        public async Task AddManagers(DialogUsersRequest request, string nickname)
        {

            await ChangeManagersRole(request, nickname, UserDialogRole.Manager);
        }

        public async Task RemoveManagers(DialogUsersRequest request, string nickname)
        {
            await ChangeManagersRole(request, nickname, UserDialogRole.User);
        }

        private async Task ChangeManagersRole(DialogUsersRequest request, string nickname, UserDialogRole role)
        {
            _check.CheckValue(request).NotNull();
            var dialog = await _db.GetGroupDialogByIdIncludeUsers(request.DialogId);
            var adminDialod = _check.UserIsFoundInDialog(nickname, dialog.Users);
            _check.CheckValue(adminDialod.Role).EqualsTo(UserDialogRole.Admin);
            
            var userDialogs = _check.UsersFoundInDialog(dialog.Users, request.Nicknames);
            userDialogs.ForEach(ud => ud.Role = role);
            
            _db.UserDialogs.UpdateRange(userDialogs);
            await _db.SaveChangesAsync();
        }
        
        private void CheckDialogUsersRequest(DialogUsersRequest request)
        {
            _check.CheckValue(request, nameof(request)).NotNull();
            _check.CheckValue(request.ManagerNickname, nameof(request.ManagerNickname)).NotEmpty();
            _check.CheckValue(request.Nicknames.Count, nameof(request.Nicknames.Count)).Greater(0);
        }
    }
}