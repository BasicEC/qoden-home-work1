using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Xml.Linq;
using HomeWork1.Constants;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models;
using HomeWork1.Models.Interfaces;
using HomeWork1.Models.Requests;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface IProfileService
    {
        Task<UserDto> GetUserProfile(string nickname);

        Task CreateProfile(UserDto userDto);

        Task UpdateProfile(ModifyProfileRequest request);
    }

    public class ProfileService : IProfileService
    {
        private PgDbContext _db;
        private Validator _check;

        public ProfileService(PgDbContext db)
        {
            _db = db;
            _check = new Validator(true);
        }

        public async Task<UserDto> GetUserProfile(string nickname)
        {
            _check.CheckValue(nickname, nameof(nickname)).NotEmpty();
            var user = await _db.GetUserByNickNameIncludeDepartment(nickname);
            _check.IsFound(user, nameof(nickname), nickname, nameof(user));
            
            return new UserDto(user);
        }

        public async Task CreateProfile(UserDto userDto)
        {
            _check.CheckValue(userDto, nameof(userDto)).NotNull();
            CheckRequiredFieldsIsNotEmpty(userDto);
            
            var user = await _db.Users.Where(u => u.Nickname == userDto.Nickname).FirstOrDefaultAsync();
            _check.NotFound(user, nameof(userDto.Nickname), userDto.Nickname, nameof(User));

            _check.CheckValue(userDto.Department, nameof(userDto.Department)).NotEmpty();
            var department = await _db.Departments.FirstOrDefaultAsync(d => d.Name == userDto.Department);
            
            _check.IsFound(department, nameof(Department.Name), userDto.Nickname, nameof(userDto.Department));

            PasswordHasher<User> hasher = new PasswordHasher<User>();
            var hashedPassword = hasher.HashPassword(null,userDto.Password);
            
            _db.Users.Add(new User
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Patronymic = userDto.Patronymic,
                Nickname = userDto.Nickname,
                Email = userDto.Email,
                PhoneNumber = userDto.PhoneNumber,
                Password = hashedPassword,
                Description = userDto.Description,
                DepartmentId = department.Id,
                InvitedAt = DateTime.Now,
            });
            await _db.SaveChangesAsync();
        }

        public async Task UpdateProfile(ModifyProfileRequest request)
        {
            _check.CheckValue(request).NotNull();
            _check.CheckValue(request.CurrentNickname, nameof(request.CurrentNickname)).NotEmpty();
            CheckFieldsIsNotEmptyOrNull(request);
            
            var user = await _db.GetUserByNickNameIncludeDepartment(request.CurrentNickname);
            _check.IsFound(user, nameof(request.CurrentNickname), request.CurrentNickname, nameof(User));
            
            var newNicknameUser = await _db.GetUserByNickName(request.Nickname);
            _check.NotFound(newNicknameUser,nameof(request.Nickname), request.Nickname, nameof(User));

            if (user.Department.Name != request.Department && request.Department != null)
            {
                var department = await _db.Departments.FirstAsync(d => d.Name == request.Department);
                _check.IsFound(department, nameof(request.Department), request.Department,nameof(Department));
                user.DepartmentId = department.Id;
            }

            user.Nickname = request.Nickname ?? user.Nickname;
            user.FirstName = request.FirstName ?? user.FirstName;
            user.LastName = request.LastName ?? user.LastName;
            user.Patronymic = request.Patronymic ?? user.Patronymic;
            user.PhoneNumber = request.PhoneNumber ?? user.PhoneNumber;
            user.Email = request.Email ?? user.Email;
            user.Description = request.Description ?? user.Description;

            _db.Users.Update(user);
            await _db.SaveChangesAsync();
        }

        private void CheckRequiredFieldsIsNotEmpty(UserDto userDto)
        {
            var validator = new Validator();
            validator.CheckValue(userDto.Nickname, nameof(userDto.Nickname)).NotEmpty();
            validator.CheckValue(userDto.FirstName, nameof(userDto.FirstName)).NotEmpty();
            validator.CheckValue(userDto.LastName, nameof(userDto.LastName)).NotEmpty();
            validator.CheckValue(userDto.Department, nameof(userDto.Department)).NotEmpty();
            validator.CheckValue(userDto.Password, nameof(userDto.Password)).NotEmpty();
            validator.CheckValue(userDto.PhoneNumber, nameof(userDto.PhoneNumber)).NotEmpty();
            validator.Throw();
        }

        private void CheckFieldsIsNotEmptyOrNull(IUserDto request)
        {
            var validator = new Validator();
            validator.NotEmptyOrNull(nameof(request.Nickname), request.Nickname);
            validator.NotEmptyOrNull(nameof(request.FirstName), request.FirstName);
            validator.NotEmptyOrNull(nameof(request.LastName), request.LastName);
            validator.NotEmptyOrNull(nameof(request.Department), request.Department);
            validator.NotEmptyOrNull(nameof(request.Email), request.Email);
            validator.Throw();
        }
    }
}