using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Microsoft.EntityFrameworkCore;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface IRateRequestService
    {
        Task CreateRequest(RateRequestRequest rateRequestRequest, int userId);
        Task<List<RateRequestDto>> GetAllRateRequests();
        Task<List<RateRequestDto>> GetAssignedRateRequests(int managerId);
        Task<List<RateRequestDto>> GetUserRateRequests(int userId);
    }

    public class RateRequestService : IRateRequestService
    {
        private PgDbContext _db;
        private Validator _check;

        public RateRequestService(PgDbContext db)
        {
            _db = db;
            _check = new Validator(true);
        }

        public async Task CreateRequest(RateRequestRequest rateRequestRequest, int userId)
        {
            _check.CheckValue(rateRequestRequest).NotNull();
            _check.CheckValue(rateRequestRequest.Rate, nameof(rateRequestRequest.Rate)).Greater(0);
            _check.NotEmptyOrNull(nameof(rateRequestRequest.Description), rateRequestRequest.Description);

            _db.CreateRequest(
                new RateRequest
                {
                    RequestId = Guid.NewGuid(),
                    UserId = userId,
                    Rate = rateRequestRequest.Rate,
                    Description = rateRequestRequest.Description,
                    CreatedAt = DateTime.Now,
                    Status = RateRequestStatus.Review,
                });
            await _db.SaveChangesAsync();
        }

        public async Task<List<RateRequestDto>> GetAllRateRequests()
        {
            return await _db.RateRequests
                .Include(r => r.Reviewer)
                .Include(r => r.User)
                .Select(r => new RateRequestDto(r))
                .ToListAsync();
        }

        public async Task<List<RateRequestDto>> GetAssignedRateRequests(int managerId)
        {
            var rateRequestDtos = await _db.GetAssignedUsersRequests(managerId);
            return rateRequestDtos.Select(r => new RateRequestDto(r)).ToList();
        }

        public async Task<List<RateRequestDto>> GetUserRateRequests(int userId)
        {
            var rateRequests = await _db
                .GetUserRateRequests(userId);
            return rateRequests
                .Select(r => new RateRequestDto(r))
                .ToList();
        }
    }
}