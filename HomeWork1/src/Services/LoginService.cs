using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HomeWork1.Constants;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models.Requests;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface ILoginService
    {
        Task<ClaimsPrincipal> LoginUser(LoginRequest request);
    }
    public class LoginService : ILoginService
    {
        private PgDbContext _db;

        public LoginService(PgDbContext db)
        {
            _db = db;
        }

        public async Task<ClaimsPrincipal> LoginUser(LoginRequest request)
        {
            const string errMsg = "Invalid nickname or password";
            Check.Value(request, nameof(LoginRequest)).NotNull();
            Check.Value(request.Nickname, nameof(request.Nickname)).NotEmpty();
            Check.Value(request.Password, nameof(request.Password)).NotEmpty();
            
            var user = await _db.Users.Where(u => u.Nickname == request.Nickname).FirstOrDefaultAsync();

            Check.Value(user).NotNull(errMsg);
            
            PasswordHasher<User> hasher = new PasswordHasher<User>();
            var verificationResult = hasher.VerifyHashedPassword(null, user.Password, request.Password);

            Check.Value(verificationResult).EqualsTo(PasswordVerificationResult.Success, errMsg);
            
            return await Authenticate(user);            
        }

        private async Task<ClaimsPrincipal> Authenticate(User user)
        {
            var roles = await _db.Roles
                .Include(r => r.UserRoles)
                .Where(r => r.UserRoles.Exists(ur => ur.UserId == user.Id))
                .ToListAsync();
            
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Nickname),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.Integer),
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return new ClaimsPrincipal(claimsIdentity);
            
        }
    }
}