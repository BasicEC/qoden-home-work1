using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models;
using HomeWork1.Repos;
using HomeWork1.Util.Validations;
using Microsoft.EntityFrameworkCore;
using Qoden.Validation;

namespace HomeWork1.Services
{
    public interface IMessageService
    {
        Task<List<string>> NewMessage(MessageDto messageDto);
    }

    public class MessageService : IMessageService
    {
        private PgDbContext _db;
        private Validator _check;

        public MessageService(PgDbContext db)
        {
            _db = db;
            _check = new Validator(true);
        }

        public async Task<List<string>> NewMessage(MessageDto messageDto)
        {
            _check.CheckValue(messageDto.Text, nameof(messageDto.Text)).NotEmpty();

            var dialog = await _db.GetGroupDialogByIdIncludeUsers(messageDto.DialogId);
            _check.IsFound(dialog, nameof(messageDto.DialogId), messageDto.DialogId.ToString(), nameof(Dialog));
            var userDialog = _check.UserIsFoundInDialog(messageDto.Sender, dialog.Users);
                
            var message = new Message
            {
                DialogId = messageDto.DialogId,
                SenderId = userDialog.UserId,
                Text = messageDto.Text,
                SentAt = messageDto.SentAt,
            };

            _db.Messages.Add(message);
            await _db.SaveChangesAsync();
            return dialog.Users.Select(ud => ud.User.Id.ToString()).ToList();
        }
    }
}