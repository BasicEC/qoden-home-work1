using System;
using System.Threading.Tasks;
using HomeWork1.Models;
using HomeWork1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace HomeWork1.Hubs
{
    [Authorize]
    public class MessageHub : Hub
    {
        private IMessageService _messageService;
        private string _nickname => Context.User.Identity.Name;

        public MessageHub(IMessageService messageService, IDialogService dialogService)
        {
            _messageService = messageService;
        }

        public async Task OnMessage(string dialogId, string text)
        {
            try
            {
                var message = new MessageDto
                {
                    DialogId = Guid.Parse(dialogId),
                    Sender = _nickname,
                    Text = text,
                    SentAt = DateTime.Now,
                };
                var usersIds = await _messageService.NewMessage(message);
                await Clients.Users(usersIds).SendAsync("onMessage", message);
            }
            catch (Exception e)
            {
                throw new HubException(e.Message);
            }
        }
    }
}