﻿using HomeWork1.Database;
using HomeWork1.Hubs;
using HomeWork1.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Qoden.Validation.AspNetCore;

namespace HomeWork1
{
    public class Startup
    {
        public IConfiguration Configuration;
        public IHostingEnvironment Environment;

        public Startup(IHostingEnvironment environment, IConfiguration configuration)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IRateRequestService, RateRequestService>();
            services.AddScoped<IManagerService, ManagerService>();
            services.AddScoped<IDialogService, DialogService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IUserDialogService, UserDialogService>();
            services.AddSignalR();
            services.AddMvc( o => { o.Filters.Add<ApiExceptionFilterAttribute>();})
                .AddJsonOptions(jsonOptions =>
                    {
                        jsonOptions.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                        jsonOptions.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new PathString("/");
                    options.LogoutPath = new PathString("/logout");
                });
            ConfigureDatabase(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
            app.UseCors(x => x.WithOrigins("http://localhost:8000").AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            app.UseSignalR(builder => builder.MapHub<MessageHub>("/ws/messages"));
        }

        public void ConfigureDatabase(IServiceCollection services)
        {
            services.AddEntityFrameworkNpgsql();
            services.AddDbContext<PgDbContext>(options =>
            {
                options.UseNpgsql(Configuration["Database:ConnectionString"]);
            });
        }
    }
}