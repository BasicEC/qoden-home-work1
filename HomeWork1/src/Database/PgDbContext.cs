using HomeWork1.Database.Models;
using HomeWork1.Extensions;
using Microsoft.EntityFrameworkCore;
namespace HomeWork1.Database
{
    public class PgDbContext : DbContext
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<ManagerAssignedUser> ManagerAssignedUsers { get; set; }
        public DbSet<RateRequest> RateRequests { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SalaryRate> SalaryRates { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Dialog> Dialogs { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<UserDialog> UserDialogs { get; set; }
        
        public PgDbContext(DbContextOptions<PgDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            this.ApplyOnModelCreatingFromAllEntities(builder);
            this.ApplySnakeCase(builder);

            //only description of relationships, all fields are described in classes 
            builder.Entity<RateRequest>()
                .HasOne(r => r.User)
                .WithMany(u => u.RateRequests)
                .HasForeignKey(r => r.UserId);

            builder.Entity<RateRequest>()
                .HasOne(r => r.Reviewer)
                .WithMany(u => u.ReviewedRateRequests)
                .HasForeignKey(r => r.ReviewerId);

            builder.Entity<Role>()
                .HasMany(r => r.UserRoles)
                .WithOne(ur => ur.Role);
            
            builder.Entity<ManagerAssignedUser>()
                .HasOne(m => m.AssignedUser)
                .WithMany(u => u.AssignedUsers)
                .HasForeignKey(m=>m.ManagerId);

            builder.Entity<ManagerAssignedUser>()
                .HasOne(m => m.Manager)
                .WithMany(u => u.Managers)
                .HasForeignKey(m => m.AssignedUserId);

            builder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.UserRoles)
                .HasForeignKey(ur => ur.UserId);
            
            builder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId);
            
            builder.Entity<User>()
                .HasMany(u => u.UserRoles)
                .WithOne(ur => ur.User);
            
            builder.Entity<User>()
                .HasOne(u => u.Department)
                .WithMany(d => d.Users)
                .HasForeignKey(u => u.DepartmentId);

            builder.Entity<Message>()
                .HasOne(m => m.Dialog)
                .WithMany(d => d.Messages)
                .HasForeignKey(m => m.DialogId);

            builder.Entity<Message>()
                .HasOne(m => m.Sender)
                .WithMany(u => u.Messages)
                .HasForeignKey(m => m.SenderId);

            builder.Entity<UserDialog>()
                .HasOne(ud => ud.User)
                .WithMany(u => u.Dialogs)
                .HasForeignKey(ud => ud.UserId);

            builder.Entity<UserDialog>()
                .HasOne(ud => ud.Dialog)
                .WithMany(d => d.Users)
                .HasForeignKey(ud => ud.DialogId);
        }
    }
}