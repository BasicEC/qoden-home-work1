using System;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public enum RateRequestStatus
    {
        Review = 0,
        Rejected = 1,
        Accepted = 2
    }
    public class RateRequest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }
        public Guid RequestId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public int Rate { get; set; }
        public DateTime CreatedAt{ get; set; }
        public string Description { get; set; }
        public int? ReviewerId { get; set; }
        public User Reviewer { get; set; }
        public string ReviewerReply { get; set; }
        public string ReviewerComment { get; set; }
        public RateRequestStatus Status { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RateRequest>()
                .HasKey(req => req.Id);
            builder.Entity<RateRequest>()
                .Property(req => req.RequestId)
                .IsRequired();
            builder.Entity<RateRequest>()
                .Property(req => req.UserId)
                .IsRequired();
            builder.Entity<RateRequest>()
                .Property(req => req.Rate)
                .IsRequired();
            builder.Entity<RateRequest>()
                .Property(req => req.Status)
                .IsRequired();
        }
    }
}