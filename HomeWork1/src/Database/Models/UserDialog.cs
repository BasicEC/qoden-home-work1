using System;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public enum UserDialogRole
    {
        Admin = 1,
        Manager = 2,
        User = 3,
    }
    public class UserDialog
    {
        public int? UserId { get; set; }
        public User User { get; set; }
        public Guid DialogId { get; set; }
        public Dialog Dialog { get; set; }
        public UserDialogRole Role { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserDialog>()
                .HasKey(ud => new { ud.UserId, ud.DialogId});
            builder.Entity<UserDialog>()
                .Property(ud => ud.Role)
                .IsRequired()
                .HasDefaultValue(UserDialogRole.User);
        }
    }
}