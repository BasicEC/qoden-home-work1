using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime InvitedAt { get; set; }
        public string Description { get; set; }
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public List<RateRequest> RateRequests { get; set; }
        public List<RateRequest> ReviewedRateRequests { get; set; }
        public List<ManagerAssignedUser> Managers { get; set; }
        public List<ManagerAssignedUser> AssignedUsers { get; set; }
        public List<UserDialog> Dialogs { get; set; }
        public List<Message> Messages { get; set; }

        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasKey(u => u.Id);
            builder.Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired();
            builder.Entity<User>()
                .Property(u => u.LastName)
                .IsRequired();
            builder.Entity<User>()
                .Property(u => u.Password)
                .IsRequired();
            builder.Entity<User>()
                .Property(u => u.PhoneNumber)
                .IsRequired();
            builder.Entity<User>()
                .Property(u => u.DepartmentId)
                .IsRequired();
            builder.Entity<User>()
                .Property(u => u.Nickname)
                .IsRequired();
            builder.Entity<User>()
                .HasIndex(u => u.Nickname)
                .IsUnique();
        }
    }
}