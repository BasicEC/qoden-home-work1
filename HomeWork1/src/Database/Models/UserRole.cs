using System;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace HomeWork1.Database.Models
{
    public class UserRole
    {
        public int? UserId { get; set; }
        public int? RoleId { get; set; }

        public User User { get; set; }
        public Role Role { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserRole>()
                .HasKey(c => new {c.RoleId, c.UserId});
        }
    }
}