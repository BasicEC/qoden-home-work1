using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using HomeWork1.Constants;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public class Role
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }

        public string Name { get; set; }
        public List<UserRole> UserRoles { get; set; }

        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Role>()
                .HasKey(r => r.Id);
            builder.Entity<Role>()
                .HasAlternateKey(r => r.Name);
        }
    }
}