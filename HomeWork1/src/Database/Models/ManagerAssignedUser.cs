using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public enum ManagerStatus
    {
        Relevant = 1,
        Irrelevant = 2,
    } 
    public class ManagerAssignedUser
    {
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public User Manager { get; set; }
        public int? AssignedUserId { get; set; }
        public User AssignedUser { get; set; }
        public ManagerStatus Status { get; set; }

        public DateTime AssignedAt { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ManagerAssignedUser>()
                .HasKey(m => m.Id);
            builder.Entity<ManagerAssignedUser>()
                .Property(m => m.Status)
                .IsRequired();
        }
    }
}