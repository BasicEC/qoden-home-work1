using System;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public class Message
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long? Id { get; set; }
        public Guid DialogId { get; set; }
        public Dialog Dialog { get; set; }
        public int? SenderId { get; set; }
        public User Sender { get; set; }
        public DateTime SentAt { get; set; }
        public string Text { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Message>()
                .HasKey(m => m.Id);
            builder.Entity<Message>()
                .Property(m => m.SenderId)
                .IsRequired();
            builder.Entity<Message>()
                .Property(m => m.DialogId)
                .IsRequired();
            builder.Entity<Message>()
                .Property(m => m.Text)
                .IsRequired();
        }
    }
}