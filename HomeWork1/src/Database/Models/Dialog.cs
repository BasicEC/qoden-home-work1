using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public enum DialogType
    {
        Personal = 1,
        Group = 2,
    }
    
    public class Dialog
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DialogType Type { get; set; }
        public List<Message> Messages { get; set; }
        public List<UserDialog> Users { get; set; }
        
        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Dialog>()
                .HasKey(d => d.Id);
            builder.Entity<Dialog>()
                .Property(d => d.Type)
                .IsRequired();
        }
    }
}