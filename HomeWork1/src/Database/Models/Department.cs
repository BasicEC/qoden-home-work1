using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public class Department
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }

        public string Name { get; set; }
        public List<User> Users { get; set; }

        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Department>()
                .HasKey(d => d.Id);
            builder.Entity<Department>()
                .HasAlternateKey(d => d.Name);
        }
    }
}