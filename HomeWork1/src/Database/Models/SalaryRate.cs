using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Database.Models
{
    public class SalaryRate
    {
        public int? Id { get; set; }
        public int Rate { get; set; }
        public int? UserId { get; set; }
        public DateTime UpdatedAt { get; set; }

        [UsedImplicitly]
        public static void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SalaryRate>()
                .HasKey(s => s.Id);
            builder.Entity<SalaryRate>()
                .Property(s => s.Rate)
                .IsRequired();
            builder.Entity<SalaryRate>()
                .Property(s => s.UserId)
                .IsRequired();
            builder.Entity<SalaryRate>()
                .Property(s => s.UpdatedAt)
                .IsRequired();
        }
    }
}