using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using HomeWork1.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Repos
{
    public static class RateRequestRepository
    {
        public static void CreateRequest(this PgDbContext db, RateRequest rateRequest)
        {
            db.RateRequests.Add(rateRequest);
        }

        public static async Task<List<RateRequest>> GetAssignedUsersRequests(this PgDbContext db, int managerId)
        {
            var rateRequests = await db.RateRequests
                .Include(r => r.User)
                .ThenInclude(u => u.Managers)
                .Where(r => r.User.Managers.Exists(m => m.ManagerId == managerId && m.Status == ManagerStatus.Relevant))
                .Include(r => r.Reviewer)
                .ToListAsync();
            return rateRequests;
        }

        public static async Task<List<RateRequest>> GetUserRateRequests(this PgDbContext db, int userId)
        {
            return await db.RateRequests
                .FromSql(
                    @"select * from rate_requests as req 
                    where req.user_id = {0}
                    and req.created_at =
                    (select max(ir.created_at) from rate_requests as ir
                    where ir.request_id = req.request_id)"
                    , userId)
                .Include(r => r.Reviewer)
                .Include(r => r.User)
                .ToListAsync();
        }
    }
}