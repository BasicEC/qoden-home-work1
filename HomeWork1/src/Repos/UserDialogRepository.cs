using System;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Repos
{
    public static class UserDialogRepository
    {
        public static async Task<UserDialog> GetUserDialogIncludeDialog(this PgDbContext db,int userId, Guid dialogId)
        {
            return await db.UserDialogs
                .Include(ud => ud.Dialog)
                .Where(ud => ud.DialogId == dialogId 
                             && ud.UserId == userId)
                .FirstOrDefaultAsync();
        }
    }
}