using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Repos
{
    public static class DialogRepository
    {
        public static async Task<Dialog> GetPersonalDialogByUsersId(this PgDbContext db, int? fUserId, int? sUserId)
        {
            return await db.Dialogs
                .Where(d => d.Type == DialogType.Personal)
                .Include(d => d.Users)
                .Where(d => d.Users.Exists(u => u.UserId == fUserId)
                            && d.Users.Exists(u => u.UserId == sUserId))
                .FirstOrDefaultAsync();
        }

        public static async Task<Dialog> GetGroupDialogByIdIncludeUsers(this PgDbContext db, Guid dialogId)
        {
            return await db.Dialogs
                .Include(d => d.Users)
                .ThenInclude(ud => ud.User)
                .FirstOrDefaultAsync(d => d.Id == dialogId);
        }

        public static async Task<List<Dialog>> GetDialogsByUserId(this PgDbContext db, int userId)
        {
            return await db.UserDialogs
                .Include(ud => ud.Dialog)
                .Where(ud => ud.UserId == userId)
                .Select(ud => ud.Dialog)
                .ToListAsync();
        }
    }
}