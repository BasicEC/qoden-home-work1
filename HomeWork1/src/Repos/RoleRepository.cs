using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Repos
{
    public static class RoleRepository
    {
        public static async Task<List<Role>> GetUserRolesById(this PgDbContext db ,int userId)
        {
            var userRoles = from ur in db.UserRoles
                join r in db.Roles on ur.RoleId equals r.Id
                where ur.UserId == userId
                select new Role() {Id = ur.RoleId, Name = r.Name};
            return await userRoles.ToListAsync();
        }
    }
}