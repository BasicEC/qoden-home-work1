using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Database;
using HomeWork1.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork1.Repos
{
    public static class UserRepository
    {
        public static async Task<User> GetUserByNickNameIncludeDepartment(this PgDbContext db ,string nickname)
        {
            return await db.Users
                .Where(u => u.Nickname == nickname)
                .Include(u => u.Department)
                .FirstOrDefaultAsync();
        }
        
        public static async Task<User> GetUserByNickName(this PgDbContext db, string nickname)
        {
            return await db.Users
                .Where(u => u.Nickname == nickname)
                .FirstOrDefaultAsync();
        }

        public static async Task<List<User>> GetUsersByNickNames(this PgDbContext db, params string[] nicknames)
        {
            return await db.Users.Where(u => nicknames.Contains(u.Nickname)).ToListAsync();
        }
    }
}