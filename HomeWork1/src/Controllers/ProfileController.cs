using System.Threading.Tasks;
using HomeWork1.Constants;
using HomeWork1.Database;
using HomeWork1.Models;
using HomeWork1.Models.Interfaces;
using HomeWork1.Models.Requests;
using HomeWork1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork1.Controllers
{
    [Route("/profile")]
    public class ProfileController : Controller
    {
        private IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IUserDto> GetProfile()
        {
            var user = await _profileService.GetUserProfile(User.Identity.Name);
            user.Description = null;
            return user;
        }

        [Authorize(Roles = RoleTypes.Admin)]
        [HttpPost]
        public async Task
            CreateProfile([FromBody] UserDto user)
        {
            await _profileService.CreateProfile(user);
        }
        
        [Authorize(Roles = RoleTypes.Admin + "," + RoleTypes.Manager)]
        [HttpPut]
        public async Task ModifyUser([FromBody] ModifyProfileRequest request)
        {
            await _profileService.UpdateProfile(request);
        }


        [Authorize(Roles = RoleTypes.Admin + "," + RoleTypes.Manager)]
        [HttpGet("current")]
        public async Task<IPrivateUserDto> GetCurrentProfile(string nickName)
        {
            return await _profileService.GetUserProfile(nickName);
        }
    }
}