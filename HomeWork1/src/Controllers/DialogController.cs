using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Models.Responses;
using HomeWork1.Services;
using HomeWork1.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork1.Controllers
{
    [Authorize]
    [Route("dialog")]
    public class DialogController : Controller
    {
        private IDialogService _dialogService;
        private IUserDialogService _userDialogService;

        public DialogController(IDialogService dialogService, IUserDialogService userDialogService)
        {
            _dialogService = dialogService;
            _userDialogService = userDialogService;
        }

        [HttpPost("group")]
        public async Task<DialogDto> CreateGroupDialog([FromBody] CreateGroupDialogRequest request)
        {
            return await _dialogService.CreateGroupDialog(request, User.Identity.Name);
        }

        [HttpPost("personal")]
        public async Task<DialogDto> CreatePersonalDialog([FromBody] CreatePersonalDialogRequest request)
        {
            return await _dialogService.CreatePersonalDialog(request, User.Identity.Name);
        }

        [HttpPut("users/add")]
        public async Task AddUsersToDialog([FromBody] DialogUsersRequest request)
        {
            await _userDialogService.AddUsers(request);
        }

        [HttpPut("users/remove")]
        public async Task RemoveUsersToDialog([FromBody] DialogUsersRequest request)
        {
            await _userDialogService.RemoveUsers(request);
        }

        [HttpGet]
        public async Task<GetDialogsResponse> GetMyDialogs()
        {
            return new GetDialogsResponse
            {
                Dialogs = await _dialogService.GetUserDialogs(ClaimsParser.GetUserId(User))
            };
        }

        [HttpPut("managers/add")]
        public async Task AddManagers([FromBody] DialogUsersRequest request)
        {
            await _userDialogService.AddManagers(request, User.Identity.Name);
        }
        
        [HttpPut("managers/remove")]
        public async Task RemoveManagers([FromBody] DialogUsersRequest request)
        {
            await _userDialogService.RemoveManagers(request, User.Identity.Name);
        }

        [HttpPut("leave")]
        public async Task LeaveDialog([FromBody] DialogRequest request)
        {
            await _dialogService.LeaveDialog(request, ClaimsParser.GetUserId(User));
        }

        [HttpDelete]
        public async Task DeleteDialog([Required] string dialogId)
        {
            var id = Guid.Parse(dialogId);
            await _dialogService.DeleteDialog(id, User.Identity.Name);
        }
    }
}