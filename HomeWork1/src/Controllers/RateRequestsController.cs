using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork1.Constants;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Models.Responses;
using HomeWork1.Services;
using HomeWork1.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork1.Controllers
{
    [Route("rateRequest")]
    public class RateRequestsController : Controller
    {
        private IRateRequestService _rateRequestService;

        public RateRequestsController(IRateRequestService rateRequestService)
        {
            _rateRequestService = rateRequestService;
        }

        [Authorize]
        [HttpPost]
        public async Task CreateRequest([FromBody] RateRequestRequest rateRequestRequest)
        {
            await _rateRequestService.CreateRequest(rateRequestRequest, ClaimsParser.GetUserId(User));
        }

        [Authorize(Roles = RoleTypes.Admin)]
        [HttpGet("all")]
        public async Task<GetRateRequestsResponse> GetAllRateRequests()
        {
            return new GetRateRequestsResponse
            {
                RateRequests = await _rateRequestService.GetAllRateRequests()
            };
        }
         
        [Authorize(Roles = RoleTypes.Admin + "," + RoleTypes.Manager)]
        [HttpGet("assigned")]
        public async Task<GetRateRequestsResponse> GetAssignedRateRequests()
        {
            return new GetRateRequestsResponse
            {
                RateRequests = await _rateRequestService.GetAssignedRateRequests(ClaimsParser.GetUserId(User))
            };

        }
        
        [Authorize]
        [HttpGet("my")]
        public async Task<GetRateRequestsResponse> GetMyRateRequests()
        {
            var rateRequestDtos =  await _rateRequestService.GetUserRateRequests(ClaimsParser.GetUserId(User));
            rateRequestDtos.ForEach((r) => { r.ReviewerComment = null;});
            return new GetRateRequestsResponse
            {
                RateRequests = rateRequestDtos
            };
        }
    }
}