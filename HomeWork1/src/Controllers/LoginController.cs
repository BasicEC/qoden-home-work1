using System.Threading.Tasks;
using HomeWork1.Models.Requests;
using HomeWork1.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork1.Controllers
{
    public class LoginController : Controller
    {

        [HttpPost("login")]
        public async Task Login([FromBody] LoginRequest request, [FromServices] ILoginService loginService)
        {
            var claimsPrincipal = await loginService.LoginUser(request);
            if (claimsPrincipal != null)
            {
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    claimsPrincipal);
            }
        }

        [Authorize]
        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("~/goodbye");
        }
    }
}