using System.Threading.Tasks;
using HomeWork1.Constants;
using HomeWork1.Models.Requests;
using HomeWork1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Qoden.Validation;

namespace HomeWork1.Controllers
{
    [Route("/manager")]
    public class ManagerController : Controller
    {

        private IManagerService _managerService;

        public ManagerController(IManagerService managerService)
        {
            _managerService = managerService;
        }

        [Authorize(Roles = RoleTypes.Admin)]
        [HttpPut("assign")]
        public async Task AssignUserToManager([FromBody] AssignRequest request)
        {
            Check.Value(request, nameof(request)).NotNull();
            await _managerService.AssignUserToManager(request.ManagerNickname, request.UserNickname);
        }
    }
}