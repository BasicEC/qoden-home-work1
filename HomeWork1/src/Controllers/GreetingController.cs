using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork1.Controllers
{
    public class GreetingController : Controller
    {
        [Authorize]
        [HttpGet("welcome")]
        public string SayWelcome()
        {
            if (User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value == "admin")
                return "Welcome Master!";
            return $"Welcome {User.Identity.Name}!";
        }

        [HttpGet("goodbye")]
        public string SayGoodbye()
        {
            return "Goodbye >:]";
        }

        [HttpGet("~/")]
        public string Home()
        {
            return "Please login";
        }
    }
}