using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork1.Database.Models;
using Qoden.Validation;

namespace HomeWork1.Util.Validations
{
    public static class ValidatorDialogExtension
    {
        
        
        public static void DialogRoleIsManagerOrAdmin(this Validator validator, UserDialog userDialog)
        {
            var condition = userDialog.Role == UserDialogRole.Admin || userDialog.Role == UserDialogRole.Manager;
            validator.CheckValue(condition, nameof(UserDialogRole))
                .IsTrue($"{userDialog.User.Nickname} isn't manager or admin of dialog", 
                    error => {error.Add("Value", userDialog.Role);});
        }
        
        public static void DialogRoleNotManagerOrAdmin(this Validator validator, UserDialog userDialog)
        {
            var condition = userDialog.Role == UserDialogRole.Admin || userDialog.Role == UserDialogRole.Manager;
            validator.CheckValue(condition, nameof(UserDialogRole))
                .IsFalse($"{userDialog.User.Nickname} is manager or admin of dialog", 
                    error => {error.Add("Value", userDialog.Role);});
        }

        public static void UserDialogsHaveNoManagersOrAdmin(this Validator validator,
            IEnumerable<UserDialog> userDialogs)
        {
            foreach (var userDialog in userDialogs)
            {
                validator.DialogRoleNotManagerOrAdmin(userDialog);
            }
        }
        
        public static void UserNotFoundInDialog(this Validator validator, string nickname, ICollection<UserDialog> users)
        {
            var user = users.FirstOrDefault(ud => ud.User.Nickname == nickname);
            var message = $"{nameof(User)} with {nameof(nickname)} equal to {nickname} already in the dialog"; 
            validator.NotFound(user, nameof(nickname), nickname, nameof(User), message);
        }
        
        public static UserDialog UserIsFoundInDialog(this Validator validator, string nickname, ICollection<UserDialog> users)
        {
            var user = users.FirstOrDefault(ud => ud.User.Nickname == nickname);
            var message = $"{nameof(User)} with {nameof(nickname)} equal to {nickname} not found in the dialog"; 
            validator.IsFound(user, nameof(nickname), nickname, nameof(User), message);
            return user;
        }
        
        public static void UsersNotFoundInDialog(this Validator validator, ICollection<UserDialog> users, IEnumerable<string> nicknames)
        {
            foreach (var nickname in nicknames)
            {
                validator.UserNotFoundInDialog(nickname, users);
            }
        }
        
        public static List<UserDialog> UsersFoundInDialog(this Validator validator, ICollection<UserDialog> users, IEnumerable<string> nicknames)
        {
            var foundUsers = new List<UserDialog>();
            foundUsers.AddRange(nicknames.Select(nickname => validator.UserIsFoundInDialog(nickname, users)));
            return foundUsers;
        }
        
        public static void GroupDialogIsFoundById(this Validator validator, Dialog dialog, Guid dialogId)
        {
            validator.IsFound(dialog, nameof(dialogId), dialogId.ToString(), nameof(Dialog));
            validator.CheckValue(dialog.Type, nameof(Dialog.Type)).EqualsTo(DialogType.Group);
        }
    }
}