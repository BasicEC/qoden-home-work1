using System.Collections.Generic;
using HomeWork1.Constants;
using HomeWork1.Database.Models;
using Qoden.Validation;

namespace HomeWork1.Util.Validations
{
    public static class ValidatorExtension
    {
        public static void IsFound(this Validator validator, object obj, string key, string value, string objName,
            string message = null)
        {
            message = message ?? $"{objName} with {key} equal to {value} not found";
            validator.CheckValue(obj, key).NotNull(message,error => error.Add("Value", value));
        }

        public static void NotFound(this Validator validator, object obj, string key, string value, string objName, 
            string message = null)
        {
            message = message ?? $"{objName} with {key} equal to {value} already exists";
            validator.CheckValue(obj, key).IsNull(message,error => error.Add("Value", value));
        }

        public static void NotEmptyOrNull(this Validator validator, string key, string value)
        {
            validator.CheckValue(string.IsNullOrWhiteSpace(value) && value != null, key)
                .IsFalse("{Key} can't be empty");
        }
    }
}