using System.Collections.Generic;
using System.Linq;
using HomeWork1.Database.Models;
using Qoden.Validation;

namespace HomeWork1.Util.Validations
{
    public static class ValidatorUserExtension
    {
        public static void UsersFound(this Validator validator,ICollection<User> users, IEnumerable<string> nicknames)
        {
            foreach (var nickname in nicknames)
            {
                var user = users.FirstOrDefault(u => u.Nickname == nickname);
                validator.IsFound(user, nameof(nickname), nickname, nameof(User));
            }
        }
    }
}