using System.Linq;
using System.Security.Claims;
using HomeWork1.Constants;

namespace HomeWork1.Util
{
    public static class ClaimsParser
    {
        public static int GetUserId(ClaimsPrincipal user)
        {
            return int.Parse(user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
        }
    }
}