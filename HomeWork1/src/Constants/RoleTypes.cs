namespace HomeWork1.Constants
{
    public static class RoleTypes
    {
        public const string Manager = "manager";
        public const string Admin = "admin";
    }
}