using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test.Extensions
{
    public static class JObjectExtensions
    {
        public static void CheckErrorsMessagesNotNull(this JObject jContent)
        {
            foreach (var error in jContent["errors"])
            {
                string.IsNullOrEmpty((string) error["message"]).Should().BeFalse();
            }
        }
        
        public static void CheckRateRequestByNicknames(this JObject jRateRequests)
        {
            var rateRequest = jRateRequests["rateRequests"]
                .FirstOrDefault(r => (string)r["userNickname"] != UserNickname);
            rateRequest.Should().BeNull();

            jRateRequests["rateRequests"].Count().Should().BeGreaterThan(0);
        }
    }
}