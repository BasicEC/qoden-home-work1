using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task AuthorizeClient(this HttpClient client, string nickname, string password)
        {
            var request = new LoginRequest
            {
                Nickname = nickname,
                Password = password,
            };

            var response = await client.PostAsJsonAsync(URIs.Login, request);
            var cookie = response.Headers.FirstOrDefault(x => x.Key.Equals("Set-Cookie"));
            client.DefaultRequestHeaders.Add("Cookie", cookie.Value);
        }

        public static async Task AuthorizeClient(this HttpClient client, Role role)
        {
            switch (role)
            {
                case Role.Admin:
                    await client.AuthorizeClient(AdminNickname, AdminPassword);
                    break;
                case Role.Manager:
                    await client.AuthorizeClient(ManagerNickname, ManagerPassword);
                    break;
                case Role.User:
                    await client.AuthorizeClient(UserNickname, UserPassword);
                    break;
            }
        }
    }
}