using System;
using System.Net;
using Microsoft.AspNetCore.SignalR.Client;

namespace HomeWork1.Test.Extensions
{
    public static class HubConnectionBuilderExtensions
    {
        public static IHubConnectionBuilder WithUri(this IHubConnectionBuilder builder, Uri uri,
            CookieCollection cookie)
        {
            return builder.WithUrl(uri, options => { options.Cookies.Add(cookie); });
        }
    }
}