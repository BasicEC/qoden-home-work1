using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;

namespace HomeWork1.Test.Extensions
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task CheckBadResponse(this HttpResponseMessage response)
        {
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.Should().NotBeNull();

            var jContent = await response.Content.ReadToJObject();
            jContent.CheckErrorsMessagesNotNull();
        }
    }
}