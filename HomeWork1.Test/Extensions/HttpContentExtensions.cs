using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace HomeWork1.Test.Extensions
{
    public static class HttpContentExtensions
    {
        public static async Task<JObject> ReadToJObject(this HttpContent content)
        {
            var contentStream = await content.ReadAsStreamAsync();
            var streamReader = new StreamReader(contentStream);
            var jsonContent = streamReader.ReadToEnd();
            return JObject.Parse(jsonContent);
        }
    }
}