using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Models;
using HomeWork1.Models.Interfaces;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Generators;
using Newtonsoft.Json.Linq;
using Xunit;
using Xunit.Abstractions;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test
{
    public class ProfileControllerTests : IClassFixture<ApiFixture>
    {
        private ApiFixture _api;

        public ProfileControllerTests(ApiFixture api)
        {
            _api = api;
        }
        
        [Fact]
        public async Task NotAuthorizedRequest()
        {
            _api.Client.DefaultRequestHeaders.Remove("Cookie");
            var response = await _api.Client.GetAsync("profile");
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
        }

        [Theory]
        [InlineData(AdminNickname, AdminPassword)]
        [InlineData(ManagerNickname, ManagerPassword)]
        [InlineData(UserNickname, UserPassword)]
        public async Task CanGetProfile(string nickname, string password)
        {
            await _api.Client.AuthorizeClient(nickname, password);
            var response = await _api.Client.GetAsync("profile");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.Should().NotBeNull();

            var user = await response.Content.ReadToJObject();
            user.ContainsKey("description").Should().BeFalse();
            ((string) user["nickname"]).Should().BeEquivalentTo(nickname);
            
            CheckRequiredProfileFieldsIsNotNull(user);
        }

        [Theory]
        [InlineData(Role.Admin, UserNickname)]
        [InlineData(Role.Manager, UserNickname)]
        public async Task CanGetCurrentProfile(Role role, string nickname)
        {
            await _api.Client.AuthorizeClient(role);
            var user = await GetProfile(nickname);
            
            user.ContainsKey("description").Should().BeTrue();
            ((string) user["nickname"]).Should().BeEquivalentTo(nickname);
            
            CheckRequiredProfileFieldsIsNotNull(user);
        }

        [Fact]
        public async Task CanCreateProfile()
        {
            await _api.Client.AuthorizeClient(Role.Admin);
            
            var request = new UserDto
            {
                FirstName = "Test",
                LastName = "Test",
                Patronymic = "Test",
                Department = "developers",
                Description = "test from xunit",
                Email = "test@test.com",
                Nickname = "tester",
                Password = "testtest",
                PhoneNumber = "88005553535",
            };

            var response = await _api.Client.PostAsJsonAsync("profile", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var profile = await GetProfile(request.Nickname);
            CheckProfileFields(profile, request);
        }

        [Theory]
        [InlineData(Role.Admin, UserNickname)]
        [InlineData(Role.Manager, UserNickname)]
        public async Task CanModifyProfile(Role role, string nickname)
        {
            await _api.Client.AuthorizeClient(role);
            var oldProfile = await GetProfile(nickname);
            
            var request = new ModifyProfileRequest
            {
                FirstName = "Test",
                LastName = "Test",
                CurrentNickname = nickname,
                Nickname = "Test",
                Department = "testers",
                Description = "xunit test modify",
                Email = "test@test.com",
                Patronymic = "Test",
                PhoneNumber = "00000",
            };

            var response = await _api.Client.PutAsJsonAsync("profile", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            
            var profile = await GetProfile(request.Nickname);
            CheckProfileFields(profile, request);

            request.CurrentNickname = request.Nickname;
            request.Nickname = (string) oldProfile["nickname"];
            request.FirstName = (string) oldProfile["firstName"];
            request.LastName = (string) oldProfile["lastName"];
            request.Email = (string) oldProfile["email"];
            request.Patronymic = (string) oldProfile["patronymic"];
            request.PhoneNumber = (string) oldProfile["phoneNumber"];
            request.Description = (string) oldProfile["description"];
            request.Department = (string) oldProfile["department"];
            response = await _api.Client.PutAsJsonAsync("profile", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData(Role.Admin, "")]
        [InlineData(Role.Manager, "NotUser")]
        [InlineData(Role.Admin, null)]
        public async Task CanNotGetCurrentProfile(Role role, string nickname)
        {
            await _api.Client.AuthorizeClient(role);
            var response = await _api.Client.GetAsync($"profile/current?nickName={nickname}");
            await response.CheckBadResponse();
        }

        [Theory]
        [ClassData(typeof(UserDtosGenerator))]
        public async Task CanNotCreateProfile(UserDto request)
        {
            await _api.Client.AuthorizeClient(Role.Admin);
            var response = await _api.Client.PostAsJsonAsync("profile", request);
            await response.CheckBadResponse();
        }

        [Theory]
        [ClassData(typeof(ModifyProfileRequestsGenerator))]
        public async Task CanNotModifyProfile(ModifyProfileRequest request)
        {
            await _api.Client.AuthorizeClient(Role.Admin);
            var response = await _api.Client.PutAsJsonAsync("profile", request);
            await response.CheckBadResponse();
        }
        private static void CheckRequiredProfileFieldsIsNotNull(JObject user)
        {
            string.IsNullOrEmpty((string) user["firstName"]).Should().BeFalse();
            string.IsNullOrEmpty((string) user["lastName"]).Should().BeFalse();
            string.IsNullOrEmpty((string) user["phoneNumber"]).Should().BeFalse();
            string.IsNullOrEmpty((string) user["department"]).Should().BeFalse();
            user.ContainsKey("password").Should().BeFalse();
        }

        private async Task<JObject> GetProfile(string nickname)
        {
           var response = await _api.Client.GetAsync($"profile/current?nickName={nickname}");
           response.StatusCode.Should().Be(HttpStatusCode.OK);
           response.Content.Should().NotBeNull();
           
           return await response.Content.ReadToJObject();
        }

        private static void CheckProfileFields(JObject jProfile, IPrivateUserDto user)
        {
            ((string) jProfile["firstName"]).Should().BeEquivalentTo(user.FirstName);
            ((string) jProfile["lastName"]).Should().BeEquivalentTo(user.LastName);
            ((string) jProfile["patronymic"]).Should().BeEquivalentTo(user.Patronymic);
            ((string) jProfile["department"]).Should().BeEquivalentTo(user.Department);
            ((string) jProfile["description"]).Should().BeEquivalentTo(user.Description);
            ((string) jProfile["email"]).Should().BeEquivalentTo(user.Email);
            ((string) jProfile["nickname"]).Should().BeEquivalentTo(user.Nickname);
            ((string) jProfile["phoneNumber"]).Should().BeEquivalentTo(user.PhoneNumber);
        }
    }
}