using System.Collections;
using System.Collections.Generic;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;

namespace HomeWork1.Test.Generators
{
    public class AssignRequestGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[]
            {
                new AssignRequest(),
            },
            new object[]
            {
                new AssignRequest
                {
                    UserNickname = "NotUser",
                    ManagerNickname = null,
                },
            },
            new object[]
            {
                new AssignRequest
                {
                    ManagerNickname = "NotManager",
                    UserNickname = null,
                },
            },
            new object[]
            {
                new AssignRequest
                {
                    ManagerNickname = UserProfiles.ManagerNickname,
                    UserNickname = "NotUser",
                }
            },
            new object[]
            {
                null,
            }
        };

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
    }
}