using System;
using System.Collections;
using System.Collections.Generic;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;

namespace HomeWork1.Test.Generators
{
    public class ModifyProfileRequestsGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[]
            {
                new ModifyProfileRequest(), 
            },
            new object[]
            {
                new ModifyProfileRequest
                {
                    CurrentNickname = UserProfiles.UserNickname,
                    Department = "NotDepartment",
                    Description = "xUnit test",
                    Email = "",
                    FirstName = "",
                    LastName = "",
                    Nickname = "",
                    Patronymic = "",
                    PhoneNumber = "",
                },
            },
            new object[]
            {
                new ModifyProfileRequest
                {
                    CurrentNickname = "NotUser",
                    Department = "testers",
                    Description = "xUnit test",
                    Email = "",
                    FirstName = "Test",
                    LastName = "Test",
                    Nickname = "Test",
                    Patronymic = "Test",
                    PhoneNumber = "Test",
                },
            },
            new object[]
            {
                null,
            }
        };

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
    }
}