using System.Collections;
using System.Collections.Generic;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;

namespace HomeWork1.Test.Generators
{
    public class LoginRequestsGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[]
            {
                new LoginRequest(),
            },
            new object[]
            {
                new LoginRequest
                {
                    Nickname = UserProfiles.UserNickname,
                    Password = UserProfiles.AdminPassword,
                },
            },
            new object[]
            {
                new LoginRequest
                {
                    Nickname = "NotNickname",
                    Password = UserProfiles.UserPassword,
                },
            },
            new object[]
            {
                null,
            }
        };

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
    }
}