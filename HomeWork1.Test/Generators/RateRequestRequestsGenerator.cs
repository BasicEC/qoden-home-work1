using System.Collections;
using System.Collections.Generic;
using HomeWork1.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;

namespace HomeWork1.Test.Generators
{
    public class RateRequestRequestsGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[]
            {
                new RateRequestRequest(),
            },
            new object[]
            {
                new RateRequestRequest
                {
                    Rate = -1,
                }, 
            },
            new object[]
            {
                new RateRequestRequest 
                {
                    Rate = 0,
                    Description = "xUnit",
                },
            },
            new object[]
            {
                null,
            }
        };
        
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
    }
}