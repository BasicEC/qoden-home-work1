using System;
using System.Collections;
using System.Collections.Generic;
using HomeWork1.Models;
using HomeWork1.Test.Constants;

namespace HomeWork1.Test.Generators
{
    public class UserDtosGenerator : IEnumerable<object[]>
    {

        private readonly List<object[]> _data = new List<object[]>
        {
           new object[]
           {
               new UserDto(),
           },
           new object[]
           {
               new UserDto
               {
                   Department = "NotDepartment",
                   FirstName = "Test",
                   LastName = "Test",
                   Nickname = UserProfiles.UserNickname,
                   Password = "",
                   PhoneNumber = "",
               },
           },
           new object[]
           {
               new UserDto
               {
                   Department = "testers",
                   FirstName = "Test",
                   LastName = "Test",
                   Nickname = "New",
                   Password = "password",
               },
           },
           new object[]
           {
               null,
           }
        };

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
    }
}