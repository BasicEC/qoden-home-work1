using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Generators;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Xunit;
using Xunit.Abstractions;

namespace HomeWork1.Test
{
    public class ManagerControllerTests : IClassFixture<ApiFixture>
    {
        private ApiFixture _api;
        private ITestOutputHelper _output;

        public ManagerControllerTests(ApiFixture api, ITestOutputHelper output)
        {
            _api = api;
            _output = output;
        }
        
        [Fact]
        public async Task NotAuthorizedRequest()
        {
            _api.Client.DefaultRequestHeaders.Remove("Cookie");
            var response = await _api.Client.PutAsJsonAsync("manager/assign", new AssignRequest());
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
        }

        [Fact]
        public async Task CanAssign()
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.Admin);
            var request = new AssignRequest
            {
                ManagerNickname = UserProfiles.ManagerNickname,
                UserNickname = UserProfiles.UserNickname,
            };

            var response = await _api.Client.PutAsJsonAsync("manager/assign", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [ClassData(typeof(AssignRequestGenerator))]
        public async Task CantAssign(AssignRequest request)
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.Admin);
            var response = await _api.Client.PutAsJsonAsync("manager/assign", request);
            await response.CheckBadResponse();
        }
    }
}