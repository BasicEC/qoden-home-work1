using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Generators;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Xunit.Abstractions;

namespace HomeWork1.Test
{
    public class LoginControllerTests : IClassFixture<ApiFixture>
    {
        private ApiFixture _api;
        private readonly ITestOutputHelper _testOutputHelper;

        public LoginControllerTests(ApiFixture api, ITestOutputHelper testOutputHelper)
        {
            _api = api;
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task UserCanLogin()
        {
            var request = new LoginRequest
            {
                Nickname = UserProfiles.AdminNickname,
                Password = UserProfiles.AdminPassword,
            };
            var response = await _api.Client.PostAsJsonAsync("login", request);

            response.StatusCode.Should().BeEquivalentTo(200);
            response.Headers.FirstOrDefault(x => x.Key.Equals("Set-Cookie")).Value.Should().NotBeNull();
        }

        [Theory]
        [ClassData(typeof(LoginRequestsGenerator))]
        public async Task InvalidUserCantLogin(LoginRequest request)
        {
            var response = await _api.Client.PostAsJsonAsync("login", request);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Content.Should().NotBeNull();

            var jContent = await response.Content.ReadToJObject();
            jContent.CheckErrorsMessagesNotNull();
        }
        
        
    }
}