using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Models;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Util;
using Microsoft.AspNetCore.SignalR.Client;
using Xunit;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test
{
    public class MessageHubTests : IClassFixture<ApiFixture>
    {
        private static readonly Uri _uri = new Uri(URIs.WsMessages);
        private const string _sendMessageMethodName = "OnMessage";

        [Fact]
        public async Task UserCanSendMessage()
        {
            const string messageText = "unit test message";

            var admin = await ClientCookies.Login(Role.Admin);
            var manager = await ClientCookies.Login(Role.Manager);
            var user = await ClientCookies.Login(Role.User);

            var response = await DialogUtils.CreateGroupDialogWithAdmin(manager.Client);
            var jContent = await response.Content.ReadToJObject();
            var dialogId = Guid.Parse((string) jContent["id"]);

            void OnMessage(MessageDto message)
            {
                message.Text.Should().Be(messageText);
                message.DialogId.Should().Be(dialogId);
                message.Sender.Should().Be(AdminNickname);
            }

            var adminConnection = await CreateAndStartSendMessageTestConnection(admin.Cookies, OnMessage);
            await CreateAndStartSendMessageTestConnection(manager.Cookies, OnMessage);
            await CreateAndStartSendMessageTestConnection(user.Cookies,
                message => throw new Exception("User mustn't get message"));

            await adminConnection.InvokeAsync(_sendMessageMethodName, dialogId, messageText);
        }

        private static async Task<HubConnection> CreateAndStartSendMessageTestConnection(CookieCollection cookies,
            Action<MessageDto> onMessage)
        {
            var connection = new HubConnectionBuilder().WithUri(_uri, cookies).Build();
            connection.On<MessageDto>(_sendMessageMethodName, onMessage);
            await connection.StartAsync();
            return connection;
        }
    }
}