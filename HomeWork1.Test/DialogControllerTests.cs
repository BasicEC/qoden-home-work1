using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Database.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Util;
using Xunit;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test
{
    public class DialogControllerTests : IClassFixture<ApiFixture>
    {
        private ApiFixture _api;
        
        public DialogControllerTests(ApiFixture api)
        {
            _api = api;
            var res = _api.Client.AuthorizeClient(UserNickname, UserPassword);
            res.Wait();
        }

        [Fact]
        public async Task UserCanCreateGroupDialog()
        {
            var request = new CreateGroupDialogRequest
            {
                Name = "Unit Test",
                Nicknames = new List<string>()
                {
                    AdminNickname,
                }
            };
            var response = await _api.Client.PostAsJsonAsync("dialog/group",request);
            await CheckCreateDialogResponse(response, request.Name, DialogType.Group);
        }

        [Fact]
        public async Task UserCanCreatePersonalDialog()
        {
            var request = new CreatePersonalDialogRequest
            {
                Name = "Unit Test",
                Nickname = AdminNickname,
            };
            var response = await _api.Client.PostAsJsonAsync("dialog/personal", request);
            await CheckCreateDialogResponse(response, request.Name, DialogType.Personal);
        }

        [Fact]
        public async Task UserCanDeleteGroupDialog()
        {
            var dialogResponse = await DialogUtils.CreateGroupDialogWithAdmin(_api.Client);
            var jContent = await dialogResponse.Content.ReadToJObject();
            var dialogId = (string) jContent["id"];
            var response = await _api.Client.DeleteAsync($"dialog?dialogId={dialogId}");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var getDialogsResponse = await _api.Client.GetAsync("dialog");
            var jDialogsResponse = await getDialogsResponse.Content.ReadToJObject();
            
            foreach (var dialog in jDialogsResponse["dialogs"])
            {
                ((string) dialog["id"]).Should().NotBe(dialogId);
            }
        }

        private static async Task CheckCreateDialogResponse(HttpResponseMessage response, string dialogName, DialogType type)
        {
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var jContent = await response.Content.ReadToJObject();
            
            jContent["id"].Should().NotBeNull();
            Guid.Parse((string) jContent["id"]);
            
            ((string) jContent["name"]).Should().BeEquivalentTo(dialogName);
            ((int) jContent["type"]).Should().Be((int) type);
        }
    }
}