namespace HomeWork1.Test.Constants
{
    public static class URIs
    {
        public const string Host = "http://localhost:5000/";
        public const string Login = Host + "login";
        public const string WsMessages = Host + "ws/messages";
        public const string GroupDialog = Host + "dialog/group";
    }
}