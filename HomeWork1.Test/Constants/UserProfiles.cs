namespace HomeWork1.Test.Constants
{
    public static class UserProfiles
    {
        public const string AdminNickname = "gallowsCalibrator";
        public const string AdminPassword = "Home";
        public const string ManagerNickname = "carcinoGeneticist";
        public const string ManagerPassword = "Blood";
        public const string UserNickname = "Flash";
        public const string UserPassword = "pikapika";

        public enum Role
        {
            Admin = 1,
            Manager = 2,
            User = 3,
        }
    }
}