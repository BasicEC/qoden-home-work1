using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using HomeWork1.Models.Requests;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test.Fixtures
{
    public class ApiFixture : IDisposable
    {
        public TestServer Server { get; set; }
        public HttpClient Client { get; set; }
        
                
        
        public ApiFixture()
        {
            var builder = SetupWebHost();
            Server = new TestServer(builder);
            Client = Server.CreateClient();

            if (Debugger.IsAttached)
                Client.Timeout = TimeSpan.FromHours(1);
        }

        public void Dispose()
        {
            Server.Dispose();
            Client.Dispose();
        }
        
        private static IWebHostBuilder SetupWebHost()
        {
            string configFile = $"{Directory.GetCurrentDirectory()}/../../../Configuration/config.json";
            return new WebHostBuilder()
                .ConfigureAppConfiguration(builder => { builder.AddJsonFile(configFile); })
                .UseEnvironment("Development")
                .UseStartup<Startup>();
        }

        
        
        
    }

    
}