using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Extensions;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test.Util
{
    public class ClientCookies
    {
        public CookieCollection Cookies { get; set; }
        public HttpClient Client { get; set; }
        
        public static async Task<ClientCookies> Login(Role role)
        {
            switch (role)
            {
                case Role.Admin:
                    return await Login(AdminNickname, AdminPassword);
                case Role.Manager:
                    return await Login(ManagerNickname, ManagerPassword);
                case Role.User:
                    return await Login(UserNickname, UserPassword);
                default:
                    return null;
            }
        }

        public static async Task<ClientCookies> Login(string nickname, string password)
        {
            var uri = new Uri(URIs.Login);
            var handler = new HttpClientHandler();
            var cookies = new CookieContainer();
            var client = new HttpClient(handler);

            handler.CookieContainer = cookies;

            await client.AuthorizeClient(nickname, password);
            return new ClientCookies{ Client = client, Cookies = cookies.GetCookies(uri)};
        }
    }
}