using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Fixtures;
using static HomeWork1.Test.Constants.UserProfiles;


namespace HomeWork1.Test.Util
{
    public class DialogUtils
    {
        public static async Task<HttpResponseMessage> CreateGroupDialogWithAdmin(HttpClient client)
        {
            var request = new CreateGroupDialogRequest
            {
                Name = "Unit Test",
                Nicknames = new List<string>()
                {
                    AdminNickname,
                }
            };
            var response = await client.PostAsJsonAsync(URIs.GroupDialog,request);
            return response;
        }
    }
}