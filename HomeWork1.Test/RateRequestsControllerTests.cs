using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using HomeWork1.Database.Models;
using HomeWork1.Models.Requests;
using HomeWork1.Test.Constants;
using HomeWork1.Test.Extensions;
using HomeWork1.Test.Fixtures;
using HomeWork1.Test.Generators;
using Newtonsoft.Json.Linq;
using Xunit;
using static HomeWork1.Test.Constants.UserProfiles;

namespace HomeWork1.Test
{
    public class RateRequestsControllerTests : IClassFixture<ApiFixture>
    {
        private ApiFixture _api;

        public RateRequestsControllerTests(ApiFixture api)
        {
            _api = api;
        }
        
        [Fact]
        public async Task NotAuthorizedRequest()
        {
            _api.Client.DefaultRequestHeaders.Remove("Cookie");
            var response = await _api.Client.GetAsync("rateRequest/my");
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
        }
        
        [Fact]
        public async Task CanCreateRateRequest()
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.Admin);
            int rate = 2000;
            string description = "xUnit";
            var request = new RateRequestRequest
            {
                Rate = rate,
                Description = description,
            };

            var response = await  _api.Client.PostAsJsonAsync("rateRequest", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var jRateRequests = await GetRequests("my");
            
            var rateRequest = jRateRequests["rateRequests"]
                .FirstOrDefault(r => (int)r["rate"] == 2000 
                                                    && (string)r["description"] == description
                                                    && (string)r["userNickname"] == AdminNickname);
            
            rateRequest.Should().NotBeNull();
            ((string) rateRequest["status"]).Should().Be(RateRequestStatus.Review.ToString());
            rateRequest["createdAt"].Should().NotBeNull();
            rateRequest["requestId"].Should().NotBeNull();
        }

        [Fact]
        public async Task CanGetMyRequests()
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.User);
            var jRateRequests = await GetRequests("my");
            jRateRequests["rateRequests"].Should().NotBeNull();
            
            jRateRequests.CheckRateRequestByNicknames();
        }
        
        [Fact]
        public async Task CanGetAllRequests()
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.Admin);
            var jRateRequests = await GetRequests("all");
            jRateRequests["rateRequests"].Should().NotBeNull();
        }
        
        [Fact]
        public async Task CanGetAssignedRequests()
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.Admin);
            var jRateRequests = await GetRequests("assigned");
            jRateRequests["rateRequests"].Should().NotBeNull();
            
            jRateRequests.CheckRateRequestByNicknames();
        }

        [Theory]
        [ClassData(typeof(RateRequestRequestsGenerator))]
        public async Task CantCreateRateRequest(RateRequestRequest request)
        {
            await _api.Client.AuthorizeClient(UserProfiles.Role.User);
            var response = await _api.Client.PostAsJsonAsync("rateRequest", request);
            await response.CheckBadResponse();
        }

        private async Task<JObject> GetRequests(string url)
        {
            var response = await _api.Client.GetAsync($"rateRequest/{url}");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.Should().NotBeNull();

            return await response.Content.ReadToJObject();
        }
    }
}