const Connect = (connection, onSuccess = () => {}, login, password) => {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (this.readyState != 4) return;
    connection.start()
      .then(() => onSuccess(connection))
      .catch(err => console.error(err.toString()));
  }

  const signInRequest = JSON.stringify({
    nickname: login,
    password: password,
  });
  xhr.open('POST', 'http://localhost:5000/login', true);
  xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  xhr.withCredentials = true;
  xhr.send(signInRequest);
  return connection;
}